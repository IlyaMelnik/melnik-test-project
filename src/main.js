import Vue from 'vue'
import App from './App.vue'
import {store} from './store';
import vuetify from './plugins/vuetify';
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(VueLodash, { name: 'custom' , lodash: lodash })

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
